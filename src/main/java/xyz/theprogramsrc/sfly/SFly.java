package xyz.theprogramsrc.sfly;

import org.bukkit.plugin.java.JavaPlugin;
import xyz.theprogramsrc.sfly.commands.MainCommand;
import xyz.theprogramsrc.sfly.listeners.UserListeners;
import xyz.theprogramsrc.tpsapi.TheProgramSrcClass;
import xyz.theprogramsrc.tpsapi.programmingUtils.MathUtil;
import xyz.theprogramsrc.tpsapi.programmingUtils.Utils;

import java.util.concurrent.TimeUnit;

public final class SFly extends JavaPlugin implements Plugin{

    public static TheProgramSrcClass tps;
    public static Plugin i;

    @Override
    public void onEnable(){
        tps = new TheProgramSrcClass(this, "sFly", "TheProgramSrc", "&6sFly>&r ", "en", true,false, false, false);
        i = this;
        tps.log("&aHabilitando plugin...", "&aEnabling plugin");
        new MainCommand();
        this.loadConfig();
        new UserListeners();
        Utils.runDelayedTask(()->tps.log("&aHabilitado correctamente", "&aEnabled correctly"), MathUtil.fairRoundedRandom(60,120));
    }

    @Override
    public void onDisable(){
        tps.log("&aDeshabilitando plugin...", "&aDisabling plugin");
        Utils.runDelayedTask(()->tps.log("&aDeshabilitado correctamente", "&aDisabled correctly"), MathUtil.fairRoundedRandom(3,10), TimeUnit.SECONDS);
    }

    @Override
    public boolean isCracked(){
        return !this.getServer().getOnlineMode();
    }

    @Override
    public void reloadFiles(){
        tps.reloadFiles();
    }

    private void loadConfig(){
        String p = "Messages.Notify";
        tps.getConfig().add(p+".AddAdmin", true);
        tps.getConfig().add(p+".RemAdmin", true);
        tps.getConfig().add(p+".EnableFly", true);
        tps.getConfig().add(p+".DisableFly", true);
    }
}
