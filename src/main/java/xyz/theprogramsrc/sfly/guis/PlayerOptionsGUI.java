package xyz.theprogramsrc.sfly.guis;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import xyz.theprogramsrc.sfly.SFly;
import xyz.theprogramsrc.tpsapi.gameManagement.items.ItemBuilder;
import xyz.theprogramsrc.tpsapi.gameManagement.items.XMaterial;
import xyz.theprogramsrc.tpsapi.gameManagement.ui.UI;
import xyz.theprogramsrc.tpsapi.gameManagement.ui.UIButton;
import xyz.theprogramsrc.tpsapi.gameManagement.user.User;
import xyz.theprogramsrc.tpsapi.programmingUtils.Utils;

public class PlayerOptionsGUI extends UI{

    private User u;

    public PlayerOptionsGUI(Player player, User user){
        super(SFly.tps, player);
        this.u = user;
    }

    @Override
    public String getTitle(){
        return this.isSpanish() ? "Users > "+this.u.getName() : "&7Usuarios > "+this.u.getName();
    }

    @Override
    public int getSize(){
        return 9;
    }

    @Override
    public void onUILoad(){
        if(this.u != null){
            String p_1 = "Messages.Notify";
            boolean notify_flyOn = getTPS().getConfig().getBoolean(p_1+".EnableFly");
            boolean notify_flyOff = getTPS().getConfig().getBoolean(p_1+".DisableFly");
            this.setItem(new UIButton(0, this.getToggleFlyItem(), e->{
                e.getPlayer().closeInventory();
                this.u.addExtraConfig("Flying", !this.u.getBooleanFromExtraConfig("Flying"));
                if(this.u.getPlayer().getAllowFlight()){
                    if(notify_flyOn) Utils.sendMessage(u.getPlayer(), getTPS().isSpanish() ? "&aModo vuelo &aActivado" : "&aFly mode &bEnabled");
                }else{
                    if(notify_flyOff) Utils.sendMessage(u.getPlayer(), getTPS().isSpanish() ? "&aModo vuelo &aDesactivado" : "&aFly mode &bDisabled");
                }
            }));
            this.setItem(new UIButton(8, this.getBackItem(), e->new UserSelector(e.getPlayer())));
        }
    }

    private ItemStack getBackItem(){
        ItemBuilder builder = new ItemBuilder(XMaterial.ARROW).setDisplayName(this.isSpanish() ? "&5Atras" : "&5Back");
        return builder.construct();
    }

    private ItemStack getToggleFlyItem(){
        ItemBuilder builder = new ItemBuilder(XMaterial.FEATHER).setDisplayName(this.isSpanish() ? "&aCambiar modo vuelo" : "&aToggle fly mode");
        builder.setLore(this.isSpanish() ?
                Utils.toArray(
                        "",
                        "&7Modo vuelo &b" + (this.u.getPlayer().getAllowFlight() ? "Activado" : "Desactivado")
                ) :
                Utils.toArray(
                        "",
                        "&7Fly mode &b"+ (this.u.getPlayer().getAllowFlight() ? "Enabled" : "Disabled")));
        return builder.construct();
    }
}
