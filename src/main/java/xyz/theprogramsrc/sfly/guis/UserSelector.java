package xyz.theprogramsrc.sfly.guis;

import org.bukkit.entity.Player;
import xyz.theprogramsrc.sfly.SFly;
import xyz.theprogramsrc.tpsapi.gameManagement.ui.ActionEvent;
import xyz.theprogramsrc.tpsapi.gameManagement.user.User;
import xyz.theprogramsrc.tpsapi.ownOptions.guis.UserBrowser;

public class UserSelector extends UserBrowser{

    public UserSelector(Player player){
        super(SFly.tps, player);
    }

    @Override
    public String getTitle(){
        return "&7sFly > "+ (this.isSpanish() ? "Usuarios" : "Users");
    }

    @Override
    public void onClick(ActionEvent actionEvent, User user){
        new PlayerOptionsGUI(actionEvent.getPlayer(), user);
    }
}
