package xyz.theprogramsrc.sfly.guis;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import xyz.theprogramsrc.sfly.SFly;
import xyz.theprogramsrc.tpsapi.gameManagement.items.HeadBuilder;
import xyz.theprogramsrc.tpsapi.gameManagement.items.ItemBuilder;
import xyz.theprogramsrc.tpsapi.gameManagement.items.XMaterial;
import xyz.theprogramsrc.tpsapi.gameManagement.ui.UI;
import xyz.theprogramsrc.tpsapi.gameManagement.ui.UIButton;
import xyz.theprogramsrc.tpsapi.gameManagement.user.User;
import xyz.theprogramsrc.tpsapi.programmingUtils.MathUtil;
import xyz.theprogramsrc.tpsapi.programmingUtils.Utils;

import java.util.ArrayList;
import java.util.Arrays;

public class MainGUI extends UI{

    private boolean expanded = false;

    public MainGUI(Player player){
        super(SFly.tps, player);
    }

    @Override
    public void onUILoad(){
        if(!this.expanded){
            this.setItem(new UIButton(11, this.getUserBrowserItem(), e->new UserSelector(e.getPlayer())));
            this.setItem(new UIButton(15, this.getMyOptionsItem(), e-> new PlayerOptionsGUI(e.getPlayer(), getUserManager().getUser(e.getPlayer().getUniqueId()))));
            this.setItem(new UIButton(26, this.getCloseItem(), e-> e.getPlayer().closeInventory()));
            this.setItem(new UIButton(22, this.getExpandItem(), e->{
                this.expanded = true;
                this.present();
            }));
        }else{
            this.setItem(new UIButton(11, this.getUserBrowserItem(), e->new UserSelector(e.getPlayer())));
            this.setItem(new UIButton(15, this.getMyOptionsItem(), e-> new PlayerOptionsGUI(e.getPlayer(), getUserManager().getUser(e.getPlayer().getUniqueId()))));
            this.setItem(new UIButton(53, this.getCloseItem(), e-> e.getPlayer().closeInventory()));
            this.setItem(new UIButton(45, this.getReloadItem(), e->SFly.i.reloadFiles()));
            this.setItem(new UIButton(49, this.getReduceItem(), e->{
                this.expanded = false;
                this.present();
            }));
        }
    }

    private ItemStack getUserBrowserItem(){
        String randomUserHead = this.getRandom();
        HeadBuilder headBuilder = new HeadBuilder(XMaterial.PLAYER_HEAD, randomUserHead).setDisplayName(this.isSpanish() ? "&aBuscador de Usuarios" : "&aUser browser");
        return headBuilder.construct();
    }

    private ItemStack getMyOptionsItem(){
        HeadBuilder builder = new HeadBuilder(XMaterial.PLAYER_HEAD, this.getPlayer().getName()).setDisplayName(this.isSpanish() ? "&aMis Opciones" : "&aMy Options");
        builder.setLore(this.isSpanish() ?
                Utils.toArray(
                        "",
                        "&7Opciones disponibles:",
                        "&b- Habilitar/Deshabilitar modo fly") :
                Utils.toArray(
                        "",
                        "&7Available options:",
                        "&b- Toggle fly mode"));
        return builder.construct();
    }

    private ItemStack getReloadItem(){
        ItemBuilder builder = new ItemBuilder(XMaterial.GREEN_WOOL).setDisplayName(this.isSpanish() ? "&aRecargar" : "&aReload").addEnchantment(Enchantment.DURABILITY).showEnchantments(false);
        return builder.construct();
    }

    private ItemStack getCloseItem(){
        ItemBuilder builder = new ItemBuilder(XMaterial.REDSTONE_BLOCK).setDisplayName(this.isSpanish() ? "&cCerrar" : "&cClose");
        return builder.construct();
    }

    private ItemStack getExpandItem(){
        return new ItemBuilder(XMaterial.TRIPWIRE_HOOK).setDisplayName(this.isSpanish() ? "&aExpandir" : "&aExpand").construct();
    }

    private ItemStack getReduceItem(){
        return new ItemBuilder(XMaterial.TRIPWIRE_HOOK).setDisplayName(this.isSpanish() ? "&aReducir" : "&aReduce").construct();
    }

    private String getRandom(){
        ArrayList<String> names = new ArrayList<>(Arrays.asList("TheProgramSrc", "Apixelados", "Notch", "Dinnerbone", "Mikecrack", "VegettaGaymer", "iFrostyzz", "DolphinKiss", "irs0", "a4client", "PapiMerk"));
        User[] users = getUserManager().getUsers();
        for(User u : users){
            if(u != null && u.getName() != null) names.add(u.getName());
        }
        names = this.names(names, this.getPlayer().getName());
        int random = MathUtil.fairRoundedRandom(0, names.size());
        return names.get(random);
    }

    private ArrayList<String> names(ArrayList<String> names, String toRem){
        if(names.contains(toRem)){
            names.remove(toRem);
            if(names.contains(toRem)){
                return this.names(names, toRem);
            }
        }
        return names;
    }

    @Override
    public String getTitle(){
        return "&7sFly";
    }

    @Override
    public int getSize(){
        return this.expanded ? 54 : 27;
    }
}
