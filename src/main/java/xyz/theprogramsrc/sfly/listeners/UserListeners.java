package xyz.theprogramsrc.sfly.listeners;

import org.bukkit.event.EventHandler;
import xyz.theprogramsrc.sfly.SFly;
import xyz.theprogramsrc.tpsapi.TPS;
import xyz.theprogramsrc.tpsapi.gameManagement.customEvents.time.Time;
import xyz.theprogramsrc.tpsapi.gameManagement.customEvents.time.TimerEvent;
import xyz.theprogramsrc.tpsapi.gameManagement.user.User;

public class UserListeners extends TPS{

    public UserListeners(){
        super(SFly.tps);
    }

    @EventHandler
    public void sync(TimerEvent e){
        if(e.getTime() == Time.TICK){
            User[] users = getUserManager().getUsers();
            for(User u : users){
                if(u != null){
                    if(u.isOnline()){
                        if(u.getBooleanFromExtraConfig("Flying") && !u.getPlayer().getAllowFlight()) u.getPlayer().setAllowFlight(true);
                        if(!u.getBooleanFromExtraConfig("Flying") && u.getPlayer().getAllowFlight()) u.getPlayer().setAllowFlight(false);
                        if((u.getPlayer().isOp() || u.getPlayer().hasPermission("sfly.admin")) && !u.isAdmin()) u.setAdmin(true);
                        u.save();
                    }
                }
            }
        }
    }
}
