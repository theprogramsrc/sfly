package xyz.theprogramsrc.sfly;

public interface Plugin{

    boolean isCracked();

    void reloadFiles();

}
