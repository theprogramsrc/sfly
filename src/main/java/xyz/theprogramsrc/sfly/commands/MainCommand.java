package xyz.theprogramsrc.sfly.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import xyz.theprogramsrc.sfly.SFly;
import xyz.theprogramsrc.sfly.guis.MainGUI;
import xyz.theprogramsrc.tpsapi.gameManagement.commands.ICommand;
import xyz.theprogramsrc.tpsapi.gameManagement.commands.Result;
import xyz.theprogramsrc.tpsapi.gameManagement.user.User;
import xyz.theprogramsrc.tpsapi.programmingUtils.Utils;

public class MainCommand extends ICommand{

    public MainCommand(){
        super(SFly.tps, "sfly");
    }

    @Override
    public Result onPlayerExecute(Player player, String[] args){
        if(getTPS().getUserManager().getUser(player.getUniqueId()).hasPermission("sfly.admin")){
            if(args.length == 0){
                new MainGUI(player);
            }else{
                if(args.length == 1){
                    return Result.INVALID_ARGS;
                }else{
                    User u = getTPS().getUserManager().getUser(Bukkit.getPlayer(args[1]).getUniqueId());
                    String arg_1 = args[0].toLowerCase();
                    String p_1 = "Messages.Notify";
                    boolean notify_addAdmin = getTPS().getConfig().getBoolean(p_1+".AddAdmin");
                    boolean notify_remAdmin = getTPS().getConfig().getBoolean(p_1+".RemAdmin");
                    boolean notify_flyOn = getTPS().getConfig().getBoolean(p_1+".EnableFly");
                    boolean notify_flyOff = getTPS().getConfig().getBoolean(p_1+".DisableFly");
                    switch(arg_1){
                        case "enable":
                            u.addExtraConfig("Flying", true).save();
                            if(notify_flyOn) Utils.sendMessage(u.getPlayer(), getTPS().isSpanish() ? "&aModo vuelo &aActivado" : "&aFly mode &bEnabled");
                            break;
                        case "disable":
                            u.addExtraConfig("Flying", false).save();
                            if(notify_flyOff) Utils.sendMessage(u.getPlayer(), getTPS().isSpanish() ? "&aModo vuelo &aDesactivado" : "&aFly mode &bDisabled");
                            break;
                        case "toggle":
                            u.addExtraConfig("Flying", !u.getBooleanFromExtraConfig("Flying")).save();
                            if(u.getPlayer().getAllowFlight()){
                                if(notify_flyOn) Utils.sendMessage(u.getPlayer(), getTPS().isSpanish() ? "&aModo vuelo &aActivado" : "&aFly mode &bEnabled");
                            }else{
                                if(notify_flyOff) Utils.sendMessage(u.getPlayer(), getTPS().isSpanish() ? "&aModo vuelo &aDesactivado" : "&aFly mode &bDisabled");
                            }
                            break;
                        case "addAdmin":
                            u.setAdmin(true);
                            u.save();
                            if(notify_addAdmin) Utils.sendMessage(u.getPlayer(), getTPS().isSpanish() ? "&aModo admin &aActivado" : "&aAdmin mode &bEnabled");
                            break;
                        case "remAdmin":
                            u.setAdmin(false);
                            u.save();
                            if(notify_remAdmin) Utils.sendMessage(u.getPlayer(), getTPS().isSpanish() ? "&aModo admin &aDesactivado" : "&aAdmin mode &bDisabled");
                            break;
                        case "toggleAdmin":
                            u.setAdmin(!u.isAdmin());
                            if(u.isAdmin()){
                                if(notify_addAdmin) Utils.sendMessage(u.getPlayer(), getTPS().isSpanish() ? "&aModo admin &aActivado" : "&aAdmin mode &bEnabled");
                            }else{
                                if(notify_remAdmin) Utils.sendMessage(u.getPlayer(), getTPS().isSpanish() ? "&aModo admin &aDesactivado" : "&aAdmin mode &bDisabled");
                            }
                            break;
                        case "reload":
                            SFly.i.reloadFiles();
                            break;
                        case "help":
                            break;
                    }
                }
            }
        }else{
            return Result.NO_PERMISSION;
        }
        return Result.COMPLETED;
    }

    @Override
    public Result onConsoleExecute(CommandSender commandSender, String[] args){
        if(args.length == 0){
            return Result.INVALID_ARGS;
        }else{
            if(args.length == 1){
                return Result.INVALID_ARGS;
            }else{
                User u = getTPS().getUserManager().getUser(Bukkit.getPlayer(args[1]).getUniqueId());
                String arg_1 = args[0].toLowerCase();
                String p_1 = "Messages.Notify";
                boolean notify_addAdmin = getTPS().getConfig().getBoolean(p_1+".AddAdmin");
                boolean notify_remAdmin = getTPS().getConfig().getBoolean(p_1+".RemAdmin");
                boolean notify_flyOn = getTPS().getConfig().getBoolean(p_1+".EnableFly");
                boolean notify_flyOff = getTPS().getConfig().getBoolean(p_1+".DisableFly");
                switch(arg_1){
                    case "enable":
                        u.addExtraConfig("Flying", "true").save();
                        if(notify_flyOn) Utils.sendMessage(u.getPlayer(), getTPS().isSpanish() ? "&aModo vuelo &aActivado" : "&aFly mode &bEnabled");
                        break;
                    case "disable":
                        u.addExtraConfig("Flying", "false").save();
                        if(notify_flyOff) Utils.sendMessage(u.getPlayer(), getTPS().isSpanish() ? "&aModo vuelo &aDesactivado" : "&aFly mode &bDisabled");
                        break;
                    case "toggle":
                        u.addExtraConfig("Flying", Boolean.toString(!u.getPlayer().getAllowFlight())).save();
                        if(u.getPlayer().getAllowFlight()){
                            if(notify_flyOn) Utils.sendMessage(u.getPlayer(), getTPS().isSpanish() ? "&aModo vuelo &aActivado" : "&aFly mode &bEnabled");
                        }else{
                            if(notify_flyOff) Utils.sendMessage(u.getPlayer(), getTPS().isSpanish() ? "&aModo vuelo &aDesactivado" : "&aFly mode &bDisabled");
                        }
                        break;
                    case "addAdmin":
                        u.setAdmin(true);
                        u.save();
                        if(notify_addAdmin) Utils.sendMessage(u.getPlayer(), getTPS().isSpanish() ? "&aModo admin &aActivado" : "&aAdmin mode &bEnabled");
                        break;
                    case "remAdmin":
                        u.setAdmin(false);
                        u.save();
                        if(notify_remAdmin) Utils.sendMessage(u.getPlayer(), getTPS().isSpanish() ? "&aModo admin &aDesactivado" : "&aAdmin mode &bDisabled");
                        break;
                    case "toggleAdmin":
                        u.setAdmin(!u.isAdmin());
                        if(u.isAdmin()){
                            if(notify_addAdmin) Utils.sendMessage(u.getPlayer(), getTPS().isSpanish() ? "&aModo admin &aActivado" : "&aAdmin mode &bEnabled");
                        }else{
                            if(notify_remAdmin) Utils.sendMessage(u.getPlayer(), getTPS().isSpanish() ? "&aModo admin &aDesactivado" : "&aAdmin mode &bDisabled");
                        }
                        break;
                    case "reload":
                        SFly.i.reloadFiles();
                        break;
                    case "help":
                        break;
                }
            }
        }
        return Result.COMPLETED;
    }

    @Override
    public String getDescription(){
        return getTPS().isSpanish() ? "Comando principal de sFly" : "sFly Main Command";
    }

    /*

    /sfly enable <player>  -  Enable fly mode
    /sfly disable <player>  -  Disable fly mode
    /sfly toggle <player>  -  Toggle fly mode
    /sfly addAdmin <player>  -  Add admin permissions
    /sfly remAdmin <player>  -  Remove admin permissions
    /sfly toggleAdmin <player>  -  Toggle admin permissions

     */
}
